update=Tue Nov 20 10:08:17 2018
last_client=eeschema
[pcbnew]
version=1
LastNetListRead=
UseCmpFile=1
PadDrill=0.600000000000
PadDrillOvalY=0.600000000000
PadSizeH=1.500000000000
PadSizeV=1.500000000000
PcbTextSizeV=1.500000000000
PcbTextSizeH=1.500000000000
PcbTextThickness=0.300000000000
ModuleTextSizeV=1.000000000000
ModuleTextSizeH=1.000000000000
ModuleTextSizeThickness=0.150000000000
SolderMaskClearance=0.000000000000
SolderMaskMinWidth=0.000000000000
DrawSegmentWidth=0.200000000000
BoardOutlineThickness=0.100000000000
ModuleOutlineThickness=0.150000000000
[pcbnew/libraries]
LibName1=rubi-conn
LibName2=rubi-discrete
LibName3=rubi-ic
LibName4=sockets
LibName5=connect
LibName6=discret
LibName7=pin_array
LibName8=divers
LibName9=smd_capacitors
LibName10=smd_resistors
LibName11=smd_transistors
LibName12=libcms
LibName13=display
LibName14=led
LibName15=dip_sockets
LibDir=../lib
[cvpcb]
version=1
NetIExt=net
[cvpcb/libraries]
EquName1=devcms
[eeschema]
version=1
LibDir=../lib
NetFmtName=
RptD_X=0
RptD_Y=100
RptLab=1
LabSize=60
[eeschema/libraries]
LibName1=rubi-conn
LibName2=rubi-cpu
LibName3=rubi-discrete
LibName4=rubi-ic
LibName5=rubi-sensors
LibName6=power
LibName7=device
LibName8=transistors
LibName9=conn
LibName10=linear
LibName11=regul
