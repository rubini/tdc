#include <io.h>
#include <time.h>
#include <string.h>
#include <tpi.h>

#if 0
struct tpi_hw tpi_hw = {
	.reset = {GPIO_NR(0, 17) /* pin 45 */, TPI_FLAG_INVERT},
	.clock = {GPIO_NR(0, 22) /* pin 30 */, TPI_FLAG_INVERT},
	.data_in = {GPIO_NR(0, 16) /* pin 40 */, },
	.data_out = {GPIO_NR(1, 13) /* pin 36 */,
		     TPI_FLAG_INVERT | TPI_FLAG_RECESSIVE_IS_0}
};
#else
struct tpi_hw tpi_hw = {
	.reset = {GPIO_NR(0, 14) /* trst */, TPI_FLAG_PULLUP},
	.clock = {GPIO_NR(0, 11) /* tdi */},
	.data_in = {GPIO_NR(0, 10) /* tck */},
	.data_out = {GPIO_NR(0, 10) /* tck */},
};
#endif

struct tpi_data tpi_data;

static uint8_t code[] = {
	#include "../../sw-avr/tiny.hex"
	/* 16 extra as a filler */
	0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55,
	0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa,
	#define CODE_SIZE (sizeof(code) - 16)
};

static const uint8_t params[2][3] = {
	{0x01, 0x06, 53}, /* same as in ../sw-avr/main.S: 5.3ms */
	{0x02, 0x05, 64}, /* other led, other timing: 6.4ms */
};


/* calib is where to store the values; result is parts per thousand */
int check_freq(struct tpi_hw *hw, int *calib)
{
	unsigned long t1, t2, to;
	int i, ret, jbit = gpio_get(GPIO_NR(1, 31));
	int err;

	/* read calibration first */
	for (i = 0; i < 3; i++) {
		ret = tpi_action(&tpi_hw, TPI_COMMAND_INIT, &tpi_data);
		if (ret >= 0)
			break;
	}
	tpi_data.address = TPI_ADDR_CALIB;
	tpi_data.len = 1;
	tpi_data.data[0] = 0x11; /* poison */
	tpi_action(&tpi_hw, TPI_COMMAND_LOAD, &tpi_data);
	*calib = tpi_data.data[0];
	tpi_action(&tpi_hw, TPI_COMMAND_FINI, &tpi_data);
	udelay(1000);

	/* Read back the gpio pin and measure the period */
	for (i = 0; i < 3; i++) {
		int gpio = tpi_hw.clock.gpio;
		to = jiffies + HZ / 10;

		/* wait for a rising edge, save in t1 */
		do t1 = jiffies;
		while (__gpio_get(gpio) && time_before(t1, to));
		do t1 = jiffies;
		while (!__gpio_get(gpio) && time_before(t1, to));

		/* again, save in t2 */
		do t2 = jiffies;
		while (__gpio_get(gpio) && time_before(t2, to));
		do t2 = jiffies;
		while (!__gpio_get(gpio) && time_before(t2, to));
	}
	err = (int)(t2 - t1) * 1000 / (jbit ? 6400 : 5300) - 1000;
	if (0) {
		printf("measured: %li, expected %i (%i / 1000)\n", t2 - t1,
		       jbit ? 6400 : 5300, err);
	}
	return err;
}

static void set_calib_value(uint8_t value)
{
	uint8_t *p = code + AVR_OSCCAL;

	*p = (*p & 0xf0) | (value & 0x0f); p++;
	*p = (*p & 0xf0) | (value >> 4);
}

static void calibrate(void)
{
	int value = 0x80;
	int step = 0x40;
	int iterations = 0;
	int calib, ret;

	/* Check first, rewrite later, so if already calibrated we skip */
	while (iterations++ < 10) {

		ret = check_freq(&tpi_hw, &calib);
		printf("hw calib: 0x%02x, error = %+i / 1000\n", calib, ret);
		if (ret < 8 && ret > -8)
			break;

		if (iterations > 1) { /* change (first time use 0x80) */
			if (ret > 0) value += step;
			if (ret < 0) value -= step;
			if (step > 1) step >>= 1;
		}
		set_calib_value(value);
		printf("reprogramming with calib value = 0x%02x\n", value);
		ret = tpi_write_binary(&tpi_hw, 0, code, CODE_SIZE);
		if (ret)
			printf("Flash write error %i\n", ret);
	}
}


void main(void)
{
	int i, j, ret, jbit;
	uint8_t *p;

	puts("tpi, 0xtest\n");

	/* There is a jumper between pins 8 (pio0_9) and 10 (pio1_31) of uext */
	gpio_dir_af(GPIO_NR(1, 31), GPIO_DIR_IN, 0,
		    GPIO_AF_GPIO | GPIO_AF_PULLUP);
	gpio_dir_af(GPIO_NR(0, 9), GPIO_DIR_OUT, 0,
		    GPIO_AF_GPIO);
	udelay(10);
	jbit = gpio_get(GPIO_NR(1, 31));
	printf("jumper is %s\n", jbit ? "out" : "in");
	/* modify immediate values in the code */
	p = code + AVR_PARAMS;
	for (j = 0; j < 3; j++) {
		*p = (*p & 0xf0) | (params[jbit][j] & 0x0f); p++;
		*p = (*p & 0xf0) | (params[jbit][j] >> 4); p++;
		printf("%02x %02x   ", p[-2], p[-1]);
	}
	printf("\n");

	while (1) {
		i = tpi_action(&tpi_hw, TPI_COMMAND_INIT, &tpi_data);
		printf("init: %i (@%04x: %02x %02x %02x)\n", i,
		       tpi_data.address, tpi_data.data[0], tpi_data.data[1],
			tpi_data.data[2]);
		udelay(100);
		if (i < 0) {
			tpi_action(&tpi_hw, TPI_COMMAND_FINI, &tpi_data);
			continue;
		}

		if (0) {
			/* Read ram, write it, read again */
			tpi_data.address = TPI_ADDR_SRAM;
			tpi_data.len = 3;
			tpi_action(&tpi_hw, TPI_COMMAND_LOAD, &tpi_data);
			printf("%02x %02x %02x\n", tpi_data.data[0],
			       tpi_data.data[1], tpi_data.data[2]);
			memset(tpi_data.data, 0x55, 3);
			tpi_action(&tpi_hw, TPI_COMMAND_STORE, &tpi_data);
			memset(tpi_data.data, 0x00, 3);
			tpi_action(&tpi_hw, TPI_COMMAND_LOAD, &tpi_data);
			printf("%02x %02x %02x\n", tpi_data.data[0],
			       tpi_data.data[1], tpi_data.data[2]);
		}

		while(1) {
			int err;

			/* Verify flash */
			err = tpi_check_binary(&tpi_hw,
					       TPI_NO_INIT | TPI_NO_FINI,
					       code, CODE_SIZE);
			printf("verify: %i byte differences\n", err);

			if (err <= 2) break;

			/* Write flash */
			err = tpi_write_binary(&tpi_hw,
					       TPI_NO_INIT | TPI_NO_FINI,
					       code, CODE_SIZE);
		}

		/* Read flash, first part, print it */
		for (i = 0; i < CODE_SIZE; i+= 16) {
			tpi_data.address = TPI_ADDR_FLASH + i;
			tpi_data.len = 16;
			ret = tpi_action(&tpi_hw, TPI_COMMAND_LOAD, &tpi_data);
			if (ret != tpi_data.len) {
				printf("read @%x: %i\n", i, ret);
				continue;
			}
			for (j =0 ; j < 16; j++)
				printf("%02x%c", tpi_data.data[j],
				       " \n"[j == 15]);
		}

		udelay(100);
		tpi_action(&tpi_hw, TPI_COMMAND_FINI, &tpi_data);
		udelay(1000);

		calibrate();

		while (1)
			;
	}
}
