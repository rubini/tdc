#include <io.h>
#include <time.h>

#define GPIO_TESTED GPIO_NR(0, 17) /* ssel on uext of TDC board */

static void test(void)
{
	int i;

	for (i = 0; i < 100; i++) {
		__gpio_set(GPIO_TESTED, 1);
		__gpio_set(GPIO_TESTED, 0);
	}
}

void main(void)
{
	unsigned long j;

	printf("%s\n", __FILE__);
	gpio_dir_af(GPIO_TESTED, GPIO_DIR_OUT, 0, GPIO_AF_GPIO);

	j = jiffies + HZ/100;
	while (1) {
		while (time_before(jiffies, j))
		       ;
		test();
		j += HZ / 100;
	}
}
