#include <io.h>
#include <time.h>

#define GPIO_TESTED GPIO_NR(0, 17) /* ssel on uext of TDC board */

static void test(void)
{
	asm(".rep 100");
	writel(1, __GPIO_WORD(GPIO_TESTED));
	writel(0, __GPIO_WORD(GPIO_TESTED));
	asm(".endr");
}

void main(void)
{
	unsigned long j;

	puts(__FILE__); puts("\n");
	gpio_dir_af(GPIO_TESTED, GPIO_DIR_OUT, 0, GPIO_AF_GPIO);

	j = jiffies + HZ/100;
	while (1) {
		while (time_before(jiffies, j))
		       ;
		test();
		j += HZ / 100;
	}
}
