#include <io.h>
#include <pwm.h>
#include <time.h>

#define GPIO_PPS	GPIO_NR(0, 16) /* ssel on uext of TDC board */

struct pwm_dev pps_pwm = {
	.gpio = GPIO_PPS,
	.cycle_mr = 2,
	.clock_hz = 1000,
	.nclocks = 1000,
	.t_on = 10.
};

void main(void)
{
	int err;

	puts(__FILE__); puts("\n");
	pwm_create(&pps_pwm, &err);
	while(1)
		;
}
