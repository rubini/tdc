#include <io.h>
#include <time.h>
#include <usb-serial.h>
#include <revision.h>

/*
 * TDC Simple Usb. Simple but with final frame format
 *
 * Internal (by nibble):  TTTTTttt SSMMMmmm
 *    T = timestamp before (20 bits)
 *    t = timestamp after (12 bits)
 *    S = sequence number
 *    M = mask before (12 bits, 9 meaningful)
 *    m = mask after (12 bits, 9 meaningful)
 *
 * External:  TTTTT-ttt:SS:MMM-mmm\n
 */

#if CONFIG_HZ != 1000 * 1000
#warning "HZ must be 1 million, for 1us resolution"
#endif

#define TDC_DEBUG GPIO_NR(0, 11)

static struct usbser tdc_usbser = {
        .flags = USBSER_FLAG_ECHO,
}, *us;


static void tdc_print(uint32_t w1, uint32_t w2)
{
	char s[64];

	unsigned int i1 = w1, i2 = w2; /* prevent printf warning */
	/* This is as simple as to print on a string and send it */
	sprintf(s, "%05x-%03x:%02x:%03x-%03x\n",
	       i1 >> 12, i1 & 0xfff, i2 >> 24, (i2 >> 12) & 0xfff, i2 & 0xfff);
	usbser_puts(us, s);
}

void main(void)
{
	uint32_t omask, nmask, oj, nj;
	uint32_t w1, w2;
	int i, seq = 0;

	printf("TDC Simple USB\n");
	printf("Build: %s\n   (on %s, %s)\n", __gitc__, __date__, __time__);
	gpio_dir_af(TDC_DEBUG, GPIO_DIR_OUT, 0, GPIO_AF_GPIO);

	us = usbser_init(&tdc_usbser);
	if (!us)
		panic(0, "Can't init usb serial\n");

	/*
	 * We kwow we are 11U24 (newer gpio), so read our 9-bit mask.
	 * We need pio0 12-16 and 20-23
	 */
	omask = 0x00f1f000; /* our mask of valid bits */
	for (i = 0; i < 31; i++)
		if (omask & (1 << i))
			gpio_dir_af(GPIO_NR(0, i),
				    GPIO_DIR_IN, 0,
				    GPIO_AF_GPIO | GPIO_AF_PULLDOWN);
	writel(~omask, __GPIO_MASK(0));

	nj = jiffies;
	nmask = readl(__GPIO_MPIN(0));
	while (1) {
		gpio_set(TDC_DEBUG, 1);
		usb_poll(us->ud);
		gpio_set(TDC_DEBUG, 0);
		oj = nj;
		omask = nmask;
		nj = jiffies;
		nmask = readl(__GPIO_MPIN(0));
		if (nmask == omask)
			continue;
		gpio_set(TDC_DEBUG, 1);
		w1 = (oj << 12) | (nj & 0xfff);
		w2 = (seq << 24) | omask | (nmask >> 12);
		seq++;
		tdc_print(w1, w2);
		gpio_set(TDC_DEBUG, 0);
	}
}
