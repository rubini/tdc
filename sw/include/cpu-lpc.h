#ifndef __CPU_H__
#define __CPU_H__

#ifdef __ASSEMBLER__

#if CONFIG_CPU_IS_17xx
#  define JIFFIES_ADDR		0x40094008
#else
#  define JIFFIES_ADDR		0x40018008
#endif

#else /* not assembler */

#include <stdint.h>
#include <hw.h>

#if defined(CONFIG_LPC11xx)
#  include <cpu-11xx.h>
#elif defined(CONFIG_LPC13xx)
#  include <cpu-13xx.h>
#elif defined(CONFIG_LPC17xx)
#  include <cpu-17xx.h>
#else
#  error "Unknown processor"
#endif

#define XTAL_FREQ  (12 * 1000 * 1000)
#define CPU_FREQ (XTAL_FREQ * CONFIG_PLL_CPU)

#endif /* __ASSEMBLER__ */
#endif /* __CPU_H__ */

