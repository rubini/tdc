#ifndef __REGS_13XX_H__
#define __REGS_13XX_H__
/*
 * 11xx and 13xx registers are mostly the same
 *
 * The main difference is in naming: uart0 vs. uart, ssp0 vs. ssp.
 * And the gpio cell, which is completely different.
 */
#include <regs-common.h>

#define REGBASE_SPI0		(0x40040000 / 4)
#define REGBASE_SPI1	-1 /* no such thing */

#endif /* __REGS_13XX_H__ */
