#ifndef __HW_H__
#define __HW_H__
#include <stdint.h>
#include <panic.h>

#if 0 /* This is the idea, but it clearly has a perfomance footprint */
  extern volatile uint32_t regs[];
#else /* Shortcut... */
#  if CONFIG_ARCH_BITS == 16
#    define regs ((volatile uint8_t *)0)
#  else
#    define regs ((volatile uint32_t *)0)
#  endif
#endif

#if CONFIG_ARCH_BITS == 32
static inline uint32_t readl(unsigned long reg)
{
	return regs[reg / 4];
}
static inline void writel(uint32_t val, unsigned long reg)
{
	regs[reg / 4] = val;
}
#else
static inline uint32_t readl(unsigned long reg)
{
	panic('R', "no readl here");
}
static inline void writel(uint32_t val, unsigned long reg)
{
	panic('W', "no writel here");
}
#endif

static inline uint16_t readw(unsigned long reg)
{
	volatile uint16_t *regs16 = (void *)regs;
	return regs16[reg / 2];
}
static inline void writew(uint16_t val, unsigned long reg)
{
	volatile uint16_t *regs16 = (void *)regs;
	regs16[reg / 2] = val;
}

static inline uint8_t readb(unsigned long reg)
{
	volatile uint8_t *regs8 = (void *)regs;
	return regs8[reg];
}
static inline void writeb(uint8_t val, unsigned long reg)
{
	volatile uint8_t *regs8 = (void *)regs;
	regs8[reg] = val;
}

#endif /* __HW_H__ */
