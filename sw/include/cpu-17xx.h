/* CPU-specific include files */

#define JIFFIES_ADDR 0x40094008 /* REG_TMR32B1TC == TIMER4 */

#ifndef __ASSEMBLER__
#include <regs-17xx.h>
#include <gpio-17xx.h>
#endif
