#ifndef __I2C_EEPROM_H__
#define __I2C_EEPROM_H__
#include <i2c.h>

struct i2cee_cfg {
	struct i2c_dev *i2c;
	int addr7;
	int pagesize;
};

struct i2cee_dev {
	struct i2cee_cfg *cfg;
	int offset;
};

#define I2CEE_CONT -1

int i2cee_read(struct i2cee_dev *dev, int offset, void *buf, int size);
int i2cee_write(struct i2cee_dev *dev, int offset, const void *buf, int size);

#endif /* __I2C_EEPROM_H__ */
