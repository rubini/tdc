-include $(CURDIR)/.config

# Remove quotes (horrible!)
ARCH=$(shell echo $(CONFIG_ARCH))

ifdef CONFIG_ARCH_AVR
  CROSS_COMPILE ?= avr-
else
  CROSS_COMPILE ?= arm-none-eabi-
endif

TESTS := $(wildcard tests-$(ARCH)/*-main.c)
BINARIES-y = $(TESTS:-main.c=.bin) $(TESTS:-main.c=.ihex)

TDC := $(wildcard tdc*-main.c)
BINARIES-y += $(TDC:-main.c=.bin)

OBJECTS  = $(BINARIES-y:.bin=.o)
ELFS     = $(BINARIES-y:.bin=)

CC =		$(CROSS_COMPILE)gcc
LD =		$(CROSS_COMPILE)ld
OBJDUMP =	$(CROSS_COMPILE)objdump
OBJCOPY =	$(CROSS_COMPILE)objcopy
SIZE =		$(CROSS_COMPILE)size
AUTOCONF = $(CURDIR)/include/generated/autoconf.h

GIT_VER = $(shell git describe --always --dirty)

# linker configuration and rule (we preprocess it). ram/flash is only for lpc
LDS-$(CONFIG_RAM_RUNNING) = lds/lpc-ram.lds
LDS-$(CONFIG_FLASH_RUNNING) = lds/lpc-flash.lds
LDS-$(CONFIG_ARCH_AVR) = lds/avr-flash.lds

ldflags-$(CONFIG_FLASH_RUNNING) = --entry=vectors 
ldflags-$(CONFIG_RAM_RUNNING) = --entry=_entry_ram
ldflags-y	+= -nostdlib -T $(LDS-y) -Wl,--gc-sections -Os -lgcc

# compiler flags
cflags-y		= -Wall -ffreestanding -O2 -Wstrict-prototypes
cflags-$(CONFIG_DEBUG_INFO) += -g
cflags-y		+= -include $(AUTOCONF) -include include/stddef.h
cflags-y		+= -include include/cpu-$(ARCH).h
cflags-y		+= -ffunction-sections -fdata-sections -fno-common
cflags-y		+= -Wno-format-zero-length
cflags-$(CONFIG_CM0)	+= -march=armv6-m -mthumb -mtune=cortex-m0
cflags-$(CONFIG_CM3)	+= -march=armv7-m -mthumb -mtune=cortex-m3
cflags-$(CONFIG_ARCH_AVR) += -mmcu=$(CONFIG_AVR_MCU)
cflags-y +=  -Iinclude -Ipp_printf -I.

### object file selection
# generic
obj-y			+= config.o
obj-y			= boot-$(ARCH).o setup.o setup-$(ARCH).o board.o
obj-y			+= lib/io.o lib/string.o
obj-y			+= lib/udelay-$(ARCH).o lib/jiffies-$(ARCH).o
obj-y			+= lib/ctype.o lib/sscanf.o lib/uuencode.o
obj-y			+= lib/rand.o lib/regs.o lib/panic.o
obj-y			+= pp_printf/printf.o pp_printf/vsprintf-full.o
obj-y			+= lib/stack.o
obj-y			+= lib/font.o lib/font-5x7v.o lib/font-5x8v.o
obj-y			+= lib/font-8x8h.o lib/font-8x16h.o lib/font-4x6v.o
obj-y			+= lib/font-16x32h.o
obj-y			+= lib/w1.o lib/w1-gpio.o lib/w1-temp.o lib/w1-eeprom.o
obj-y			+= lib/tc.o lib/ltc2428.o
obj-y			+= lib/i2c-eeprom.o lib/tpi.o
obj-y			+= lib/n5110.o

# logic cells (drivers)
obj-y			+= lib/io-$(ARCH).o lib/uart-$(ARCH).o

obj-$(CONFIG_LPC11xx)   += lib/gpio-11xx.o lib/pwm-lpc.o
obj-$(CONFIG_LPC11xx)	+= lib/pll.o lib/i2c.o lib/spi.o
obj-$(CONFIG_LPC11xx)	+= lib/usb-11xx.o

obj-$(CONFIG_LPC13xx)   += lib/gpio-13xx.o lib/pwm-lpc.o
obj-$(CONFIG_LPC13xx)	+= lib/pll.o lib/i2c.o lib/spi.o

obj-$(CONFIG_LPC17xx)   += lib/gpio-17xx.o # lib/pwm-lpc.o
obj-$(CONFIG_LPC17xx)   += lib/pll-17xx.o
obj-$(CONFIG_LPC17xx)   += lib/usb-17xx.o

obj-$(CONFIG_ARCH_AVR)	+= lib/gpio-avr.o

# higher level libraries (that won't fit in AVR memory)
obj-$(CONFIG_USB)	+= lib/usb-serial.o lib/usb-eth.o
obj-$(CONFIG_ARCH_LPC)	+= lib/neopixel.o lib/spi-nor.o
obj-$(CONFIG_ARCH_LPC)	+= lib/net.o lib/icmp.o lib/arp.o
obj-$(CONFIG_ARCH_LPC)	+= lib/udp.o lib/ipv4.o lib/bootp.o
obj-$(CONFIG_ARCH_LPC)	+= lib/command.o


# eventually, really build rules
all: tests-$(ARCH)/.gitignore tools tools-tdc $(LDS-y) $(ELFS) $(BINARIES-y)

.PHONY: all tools clean

tests-$(ARCH)/.gitignore: $(TESTS)
	@cp tests-$(ARCH)/.gitignore.head $@
	@cd tests-$(ARCH) && ls -1 *-main.c | sed s'/-main.c$$//' >> .gitignore
gitignore: 
tools:
	$(MAKE) -s -C tools

tools-tdc:
	$(MAKE) -s -C tools-tdc

clean:
	$(MAKE) -C tools clean
	rm -f *.o lib/*.o tests-$(ARCH)/*.o $(BINARIES-y) $(ELFS) $(LDS-y)
	rm -f *.bin tests-$(ARCH)/*.bin

%.ihex: %.bin
	${OBJCOPY} -I binary -O ihex $^ $@

%.bin: %
	${OBJCOPY} -O binary $^ $@
ifeq ("$(CONFIG_FLASH_RUNNING)", "y")
	./tools/fix-checksum $@
endif

%: %.o
	$(CC) $(cflags-y) -D__GITC__="\"$(GIT_VER)\"" -c revision.c
	$(CC) $(cflags-y) -o $@ revision.o $*.o \
		-Wl,-Map=$@.map $(ldflags-y)


config.c: .config
	echo "char *__current_config = \"\\" > $@
	grep CONFIG .config | sed -e 's/$$/\\n\\/' -e 's/"/\\"/g' >> $@
	echo "\";" >> $@


tests-$(ARCH)/%.o: $(obj-y) tests-$(ARCH)/%-main.o
	$(LD) -r $(obj-y) tests-$(ARCH)/$*-main.o -T lds/bigobj.lds -o $@

%.o: $(obj-y) %-main.o
	$(LD) -r $(obj-y) $*-main.o -T lds/bigobj.lds -o $@


%.o: %.c
	$(CC) $(cflags-y) -c $*.c -o $@

%.o: %.S
	$(CC) $(cflags-y) -c $*.S -o $@

%.i: %.c
	$(CC) $(cflags-y) -E $*.c -o $@

%.S: %.c
	$(CC) $(cflags-y) -S $*.c -o $@

lds/%.lds: lds/%.lds.S .config
	$(CC) $(cflags-y) -E -P lds/$*.lds.S -o $@

# following targets from Makefile.kconfig
silentoldconfig:
	@mkdir -p include/config
	$(MAKE) quiet=quiet_ -f Makefile.kconfig $@

scripts_basic config:
	$(MAKE) quiet=quiet_ -f Makefile.kconfig $@

%config:
	$(MAKE) quiet=quiet_ -f Makefile.kconfig $@

defconfig:
	$(MAKE) quiet=quiet_ -f Makefile.kconfig lpc11u_defconfig

.config: silentoldconfig

# This forces more compilations than needed, but it's useful
# (we depend on .config and not on include/generated/autoconf.h
# because the latter is touched by silentoldconfig at each build)
$(obj-y): .config $(wildcard include/*.h)

