#include <time.h>
#include <string.h>
#include <errno.h>
#include <i2c.h>
#include <i2c-eeprom.h>

int i2cee_read(struct i2cee_dev *dev, int offset, void *buf, int size)
{
	int ret;
	uint8_t wbuf[2];
	struct i2cee_cfg *cfg = dev->cfg;

	if (offset == I2CEE_CONT)
		offset = dev->offset;
	wbuf[0] = offset >> 8;
	wbuf[1] = offset & 0xff;

	ret = i2c_write(cfg->i2c, cfg->addr7, wbuf, sizeof(wbuf),
			I2C_FLAG_NO_STOP);
	if (ret)
		return ret;
	ret = i2c_read(cfg->i2c, cfg->addr7, buf, size,
		       I2C_FLAG_NO_START | I2C_FLAG_REPSTART);
	dev->offset = offset + size;
	return ret;
}

int i2cee_write(struct i2cee_dev *dev, int offset, const void *buf, int size)
{
	int ret;
	static uint8_t wbuf[34];
	struct i2cee_cfg *cfg = dev->cfg;

	if (size > ARRAY_SIZE(wbuf) - 2)
		return -E2BIG;
	
	if (offset == I2CEE_CONT)
		offset = dev->offset;
	wbuf[0] = offset >> 8;
	wbuf[1] = offset & 0xff;
	memcpy(wbuf + 2, buf, size);
	ret = i2c_write(cfg->i2c, cfg->addr7, wbuf, 2 + size, 0);
	if (ret)
		return ret;
	dev->offset = offset + size;
	return ret;
}
