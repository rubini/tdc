/*
 * SPI interface for LPC-1343
 * Alessandro Rubini, 2013 GNU GPL2 or later
 */
#include <io.h>
#include <time.h>
#include <spi.h>

/*
 * Static configuration structures to differentiate between spi0/spi1
 */
const static unsigned long spi_addr[] = {
	REGBASE_SPI0,
	REGBASE_SPI1,
};

const static uint32_t spi_power_bit[] = {
	REG_AHBCLKCTRL_SSP0,
	REG_AHBCLKCTRL_SSP1,
};

const static uint32_t spi_reset_bit[] = {
	0x01, 0x04,
};

const static unsigned long spi_clkdiv[] = {
	REG_SSP0CLKDIV,
	REG_SSP1CLKDIV,
};

struct spi_pin_cfg {
	uint8_t gpio;
	uint8_t af;
};

enum {MISO = 0, MOSI, SCK}; /* they are input our output if master or slave */
const static struct spi_pin_cfg spi_pins[][3] = {
	{
		/* Config shared between 11U35 and 1343 */
		[MISO] = {GPIO_NR(0, 8), GPIO_AF(1)},
		[MOSI] = {GPIO_NR(0, 9), GPIO_AF(1)},
		[SCK]  = {GPIO_NR(0,10), GPIO_AF(2)},
	}, {
		/* Config for 11U35 and 1347 -- 1343 has no SSP1 */
		[MISO] = {GPIO_NR(1,21), GPIO_AF(2)},
		[MOSI] = {GPIO_NR(0,21), GPIO_AF(2)},
		[SCK]  = {GPIO_NR(1,20), GPIO_AF(2)},
	}
};

const struct spi_pin_cfg spi_ssel[][2] = {
	{
		{GPIO_NR(0, 2), GPIO_AF(1) /* pin 10 */},
	},
	{
		{GPIO_NR(1, 23), GPIO_AF(2) /* pin 18 */},
		{GPIO_NR(1, 19), GPIO_AF(2) /* pin 2 */},
	}
};


/*
 * Code starts here
 */
struct spi_dev *spi_create(struct spi_dev *dev)
{
	const struct spi_cfg *cfg = dev->cfg;
	const struct spi_pin_cfg *pins;
	unsigned long base;
	int freq, div, d1, d2; /* clock divisors */
	uint32_t cr;
	int n = cfg->devn, hwcs = -1, slave = 0, dir_mosi, dir_miso;
	int bits = cfg->bits;

	if (CONFIG_SPI_IS_VERBOSE)
		printf("%s: dev %i, gpio %i, flags %08lx\n", __func__,
		       n, cfg->gpio_cs, cfg->flags);

	dev->current_freq = 0;

	if (n >= ARRAY_SIZE(spi_addr))
		return NULL;
	base = spi_addr[n];
	if (base == (unsigned long)-1)
		return NULL;
	dev->base = base;
	if (!bits)
		bits = 8; /* earlier there was no "bits" cfg value */
	if (bits < 4 || bits > 16)
		return NULL;

	if (cfg->flags & SPI_CFG_SLAVE)
		slave = 1;

	if (__SPI_IS_HW_CS(cfg->gpio_cs)) {
		hwcs = __SPI_GET_HW_CS(cfg->gpio_cs);

		if (hwcs >= ARRAY_SIZE(spi_ssel[0]))
			return NULL;
		if (spi_ssel[n][hwcs].af == 0)
			return NULL;
	} else {
		if (slave)
			return NULL; /* slave requires hwcs */
	}

	dir_mosi = GPIO_DIR_OUT;
	dir_miso = GPIO_DIR_IN;
	if (slave) {
		dir_mosi = GPIO_DIR_IN;
		dir_miso = GPIO_DIR_OUT;
	}

	/* Power on the device */
	regs[REG_AHBCLKCTRL] |= spi_power_bit[n];

	/*
	 * We must enable the clock, with a divider in range 1..255.
	 * Then we hava a prescaler (2..254) and a clock rate (1..256).
	 * To get somethin meaninful, and not too complex, divide by 1
	 * here, and calculate the two 8-bit values later
	 */
	regs[spi_clkdiv[n]] = 1;

	/* So, prescaler is 2..254 (only even), try to approximate rate */
	freq = cfg->freq;
	if (freq < 1000)
		freq = 1000; /* 1kHz instead of 0 or invalid negatives */
	div = (CPU_FREQ + (freq / 2)) / cfg->freq;

	if (freq > CPU_FREQ / 2) {
		/* go as fast as possible, as user requested more */
		d1 = 1;
		d2 = 2;
	} else {
		for (d2 = 2; d2 < 254; d2 += 2) {
			d1 = (div + (d2 / 2)) / d2;
			if (d1 < 257)
				break;
		}
	}
	/* FIXME: assign current freq */

	/* Reset the device (syscon block) */
	regs[REG_PRESETCTRL] &= ~spi_reset_bit[n];
	udelay(50);
	regs[REG_PRESETCTRL] |= spi_reset_bit[n];

	/* FIXME: select alternate pins */

	pins = spi_pins[n];
	gpio_dir_af(pins[MISO].gpio, dir_miso, 0, pins[MISO].af);
	gpio_dir_af(pins[MOSI].gpio, dir_mosi, 0, pins[MOSI].af);
	gpio_dir_af(pins[SCK].gpio, dir_mosi, 0, pins[SCK].af);

	/* Configure the CS GPIO, or hardware CS */
	if (__SPI_IS_HW_CS(cfg->gpio_cs)) {
		const struct spi_pin_cfg *p = spi_ssel[n] + hwcs;
		gpio_dir_af(p->gpio, dir_mosi, 0, p->af);
	} else {
		gpio_dir_af(cfg->gpio_cs, GPIO_DIR_OUT, 1, GPIO_AF_GPIO);
	}

	cr = (bits - 1) | (cfg->phase << 7) | (cfg->pol << 6) |
		((d1 - 1) << 8);
	regs[base + REGOFF_SSPCR1] = 0 /* disabled */;
	regs[base + REGOFF_SSPCR0] = cr;
	regs[base + REGOFF_SSPCPSR] = d2; /* calculated above */
	cr = slave ? 0x4 : 0;
	regs[base + REGOFF_SSPCR1] = cr;
	regs[base + REGOFF_SSPCR1] = cr | 0x2 /* SSE - enable */;

	if (CONFIG_SPI_IS_VERBOSE)
		printf("%s: success: dev %i, gpio %i = %p\n", __func__,
		       n, cfg->gpio_cs, dev);

	return dev;
}

void spi_destroy(struct spi_dev *dev)
{
	int n = dev->cfg->devn;
	const struct spi_pin_cfg *pins;

	/* De-configure the GPIO pins (back to gpio mode) */
	pins = spi_pins[n];
	gpio_dir_af(pins[MISO].gpio, GPIO_DIR_IN, 0, GPIO_AF_GPIO);
	gpio_dir_af(pins[MOSI].gpio, GPIO_DIR_IN, 0, GPIO_AF_GPIO);
	gpio_dir_af(pins[SCK].gpio, GPIO_DIR_IN, 0, GPIO_AF_GPIO);

	/* Configure the CS GPIO, or hardware CS */
	if (__SPI_IS_HW_CS(dev->cfg->gpio_cs)) {
		int hwcs = __SPI_GET_HW_CS(dev->cfg->gpio_cs);
		const struct spi_pin_cfg *p = spi_ssel[n] + hwcs;
		gpio_dir_af(p->gpio, GPIO_DIR_IN, 0, GPIO_AF_GPIO);
	} else {
		gpio_dir_af(dev->cfg->gpio_cs, GPIO_DIR_IN, 0, GPIO_AF_GPIO);
	}

	/* Power off the device */
	regs[REG_AHBCLKCTRL] &= ~spi_power_bit[n];

	dev->current_freq = 0;
}

/* Local functions to simplify xfer code */
static int __spi_wait_busy(unsigned long base, int jiffies_timeout)
{
	unsigned long j = jiffies + jiffies_timeout;

	while (1) {
		if (regs[base + REGOFF_SSPSR] & REGOFF_SSPSR_RNE)
			return 0;
		if (time_after_eq(jiffies, j))
			break;
	}
	return -1;
}

static void __spi_cs(struct spi_dev *dev, int value)
{
	gpio_set(dev->cfg->gpio_cs, value);
}

/* The core of the slave: we have data to provide, pre-fill the fifo */
static int spi_slave_xfer(struct spi_dev *dev,
			  const struct spi_ibuf *ibuf,
			  struct spi_obuf *obuf)
{
	unsigned long base = dev->base;
	unsigned long j;
	uint8_t *indata8 = ibuf->buf;
	uint16_t *indata16 = ibuf->buf16;
	int i, done = 0, ilen, olen, bits = dev->cfg->bits ?: 8;

	/*
	 * As a slave, we must fill the fifo, updating obuf. The return
	 * value is the successfully-transfered count; written to ibuf.
	 * We don't update ibuf: the caller must do it as usual in read(),
	 * but ilen must be specified, even if ibuf->buf is NULL;
	 */
	ilen = ibuf->len;
	olen = obuf ? obuf->len : -1;
again:
	if (olen >= 0) {
		for (i = 0; i < olen &&
			     (regs[base + REGOFF_SSPSR] & REGOFF_SSPSR_TNF);
		     i++) {
			if (bits > 8)
				regs[base + REGOFF_SSPDR] = obuf->buf16[i];
			else
				regs[base + REGOFF_SSPDR] = obuf->buf[i];
		}
		obuf->buf += i * (bits > 8);
		obuf->len -= i;
		olen -= i;
	} else {
		/* no output, just fill with 0xff until full */
		while (regs[base + REGOFF_SSPSR] & REGOFF_SSPSR_TNF)
			regs[base + REGOFF_SSPDR] = 0xffff;
	}

	/* Note: timeout can be zero: slaves must be timeout-aware */
	j = jiffies + dev->cfg->timeout;

	for (i = 0; i < ilen; i++) {
		uint16_t val;
		if (!(regs[base + REGOFF_SSPSR] & REGOFF_SSPSR_RNE))
			break;
		val = regs[base + REGOFF_SSPDR];
		if (indata8) {
			if (bits > 8)
				*(indata16++) = val;
			else
				*(indata8++) = val;
		}
	}
	done += i;
	if (time_before(jiffies, j))
		goto again;
	return done;
}

/* And the core of the master, used as only external entry point */
int spi_xfer(struct spi_dev *dev,
		     enum spi_flags flags,
		     const struct spi_ibuf *ibuf,
		     struct spi_obuf *obuf)
{
	int i, len, err = 0;
	uint16_t valo, vali;
	unsigned long base;
	int timeout = dev->cfg->timeout ?: SPI_DEFAULT_TIMEOUT;
	int bits = dev->cfg->bits ?: 8;

	if (!ibuf && !obuf)
		return 0; /* nothing to do */

	if (dev->cfg->flags & SPI_CFG_SLAVE)
		return spi_slave_xfer(dev, ibuf, obuf);

	/* if it's both input and output, lenght must be the same */
	if (ibuf && obuf) {
		if (ibuf->len != obuf->len)
			return -1; /* EINVAL ... */
	}
	len = ibuf ? ibuf->len : obuf->len;
	base = dev->base;

	if (__SPI_IS_HW_CS(dev->cfg->gpio_cs))
		flags |= SPI_F_NOCS;

	if (CONFIG_SPI_IS_VERBOSE)
		printf("%s(%p): %s", __func__, dev,
		       flags & SPI_F_NOINIT ? "--" : "cs");

	if ( !(flags & SPI_F_NOINIT) ) {
		udelay(CONFIG_SPI_CS_DELAY);
		__spi_cs(dev, 0);
		udelay(CONFIG_SPI_CS_DELAY);
	}
	for (i = 0; i < len; i++) {
		if (!obuf)
			valo = 0xffff;
		else if (bits > 8)
			valo = obuf->buf16[i];
		else
			valo = obuf->buf[i];
		regs[base + REGOFF_SSPDR] = valo;
		err =__spi_wait_busy(base, timeout);
		if (err)
			break;
		vali = regs[base + REGOFF_SSPDR];
		if (CONFIG_SPI_IS_VERBOSE)
			printf(" %04x(%02x)", valo, vali);
		if (ibuf && bits > 8)
			ibuf->buf16[i] = vali;
		else if (ibuf)
			ibuf->buf[i] = vali;
	}

	if (err) {
		len = i;
		if (!len)
			len = -1;
		if (dev->cfg->flags & SPI_CFG_VERBOSE)
			printf("%s: timeout\n", __func__);
		if (CONFIG_SPI_IS_VERBOSE)
			printf(" E!");
	}

	if (CONFIG_SPI_IS_VERBOSE)
		printf(" %s\n", flags & SPI_F_NOFINI ? "--" : "cs");

	if ( !(flags & SPI_F_NOFINI) ) {
		udelay(CONFIG_SPI_CS_DELAY);
		__spi_cs(dev, 1);
		udelay(CONFIG_SPI_CS_DELAY);
	}
	return len;
}

/* FIXME: we need an spi_poll function for slaves */
