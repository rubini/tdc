#include <io.h>
#include <errno.h>

void putc(int c)
{
	if (c == '\n')
		putc('\r');
	while ( !(regs[REG_U0LSR] & REG_U0LSR_THRE) )
		;
	regs[REG_U0THR] = c;
}

/* returns negative on failure. But \r\n is two bytes, remember it */
static int pending = -1;
int try_putc(int c)
{

	if ( !(regs[REG_U0LSR] & REG_U0LSR_THRE) )
		return -1;
	if (pending >= 0) {
		regs[REG_U0THR] = pending;
		pending = -1;
		return 0;
	}
	if (c == '\n') {
		pending = c;
		regs[REG_U0THR] = '\r';
		return -1;
	}
	regs[REG_U0THR] = c;
	return 0;
}

/* getc is blocking */
int getc(void)
{
	while (!(regs[REG_U0LSR] & REG_U0LSR_RDR))
		;
	return regs[REG_U0RBR];
}

/* non-blocking serial input, returns NULL or -EAGAIN if not ready */
int pollc(void)
{
	int ret = -EAGAIN;
	if (regs[REG_U0LSR] & REG_U0LSR_RDR)
		return regs[REG_U0RBR];
	return ret;
}

