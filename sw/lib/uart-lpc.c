#include <init.h>
#include <panic.h>

/* Hack: define registers we refer to, in false conditionals */
#ifndef REG_UARTCLKDIV
#define REG_UARTCLKDIV 0
#endif
#ifndef REG_PCLKSEL0
#define REG_PCLKSEL0 0
#endif

static int uart_init(void)
{
	unsigned int val;

	/* Turn on the clock for the uart */
	regs[REG_AHBCLKCTRL] |= REG_AHBCLKCTRL_UART;

	/* Turn on the clock for pin configuration, and gpio too */
	regs[REG_AHBCLKCTRL] |= REG_AHBCLKCTRL_IOCON | REG_AHBCLKCTRL_GPIO;

	/* First fix pin configuration */
	if (CONFIG_CPU_IS_11U || CONFIG_CPU_IS_1347) {
		gpio_dir_af(GPIO_NR(0, 18),
			    GPIO_DIR_IN,  0, GPIO_AF(1)); /* 0-18: rx */
		gpio_dir_af(GPIO_NR(0, 19),
			    GPIO_DIR_OUT, 0, GPIO_AF(1)); /* 0-19: tx */
	} else if (CONFIG_CPU_IS_13xx) {
		/* 1311, 1313, 1342, 1343 but also old series 11xx */
		gpio_dir_af(GPIO_NR(1, 6),
			    GPIO_DIR_IN,  0, GPIO_AF(1)); /* 1-6: rx */
		gpio_dir_af(GPIO_NR(1, 7),
			    GPIO_DIR_OUT, 0, GPIO_AF(1)); /* 1-7: tx */
	} else if (CONFIG_CPU_IS_17xx) {
		gpio_dir_af(GPIO_NR(0, 3),
			    GPIO_DIR_IN,  0, GPIO_AF(1)); /* 0-3: rx */
		gpio_dir_af(GPIO_NR(0, 2),
			    GPIO_DIR_OUT, 0, GPIO_AF(1)); /* 0-2: tx */
	} else {
		/* Try to get a build error */
		(void)sizeof(char[ -1 +
				   CONFIG_CPU_IS_11U + CONFIG_CPU_IS_1347 +
				   CONFIG_CPU_IS_13xx + CONFIG_CPU_IS_17xx]);
		panic(0, "Unsupported CPU");
	}

	/*
	 * The clock divider must be set before turning on the clock.
	 * If we start from 12MHz, we would divide by 3.25 to get 230400:
	 * 12M / 230400 / 16 = 3.255208
	 */
	if (!CONFIG_CPU_IS_17xx) {
		/* This register is missing in 17xx, so we multiply later */
		regs[REG_UARTCLKDIV] = CONFIG_PLL_CPU; /* Back to 12MHz */
	} else {
		/* Ensure we are *not* dividing the clock */
		regs[REG_PCLKSEL0] = (regs[REG_PCLKSEL0] & ~0xc0) | 0x40;
	}

	/* Disable interrupts and clear pending interrupts */
	regs[REG_U0IER] = 0;
	val = regs[REG_U0IIR];
	val = regs[REG_U0RBR];
	val = regs[REG_U0LSR];

	/* Set bit rate: enable DLAB bit and then divisor */
	regs[REG_U0LCR] = 0x80;

	/*
	 * This calculation is hairy: we need 6.5 or a multiple, but the
	 * fractional divisor register makes (1 + A/B) where B is 1..15
	 * 3.255208 = 2 * 1.627604 =~ 2 * 1.625 = 2 * (1+5/8)
	 */
	val = 2;
	/* Support sub-multiples of 230400 */
	val *= (230400 / CONFIG_UART_SPEED);

	/* Then, ensure with a build-time error that the speed is ok */
#if 230400 % CONFIG_UART_SPEED
#error "Invalid uart speed value in CONFIG_UART_SPEED"
#endif

	if (CONFIG_CPU_IS_17xx)
		val *= CONFIG_PLL_CPU;

	regs[REG_U0DLL] = val & 0xff;
	regs[REG_U0DLM] = (val >> 8) & 0xff;
	regs[REG_U0FDR] = (8 << 4) | 5; /* 1 + 5/8 */

	/* clear DLAB and write mode (8bit, no parity) */
	regs[REG_U0LCR] = 0x3;
	regs[REG_U0FCR] = 0x1;
	return 0;
}

rom_initcall(uart_init);

