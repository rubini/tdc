#include <io.h>
#include <i2c.h>
#include <time.h>
#include <errno.h>

struct i2c_dev *i2c_create(struct i2c_dev *dev)
{
	gpio_dir_af(dev->cfg->gpio_scl, GPIO_DIR_OUT, 1, GPIO_AF_GPIO);
	gpio_dir_af(dev->cfg->gpio_sda, GPIO_DIR_OUT, 1, GPIO_AF_GPIO);
	return dev;
}

/* To enable async operation, describe all actions */
enum i2c_op {
	OP_DONE = 0,
	OP_SET_SCL,
	OP_CLR_SCL,
	OP_SET_SDA,
	OP_CLR_SDA,
	OP_WR_SDA, /* and shifts value */
	OP_RD_SDA, /* and shifts value */
	OP_ACK_SDA, /* 1 or 0 */
};

/* Operations are sequences of bit I/O */
static const uint8_t i2c_op_start[] = {
	OP_CLR_SDA,
	OP_CLR_SCL,
	OP_DONE
};

static const uint8_t i2c_op_repstart[] = {
	OP_SET_SDA,
	OP_SET_SCL,
	OP_CLR_SDA,
	OP_CLR_SCL,
	OP_DONE
};

static const uint8_t i2c_op_stop[] = {
	OP_CLR_SDA,
	OP_SET_SCL,
	OP_SET_SDA,
	OP_DONE
};

static const uint8_t i2c_op_clk1[] = {
	OP_SET_SCL,
	OP_DONE
};

static const uint8_t i2c_op_reset[] = { /* start, then stop */
	OP_CLR_SDA,
	OP_CLR_SCL,
	OP_SET_SCL,
	OP_SET_SDA,
	OP_DONE
};

static const uint8_t i2c_op_write_byte[] = {
	OP_WR_SDA, OP_SET_SCL, OP_CLR_SCL,
	OP_WR_SDA, OP_SET_SCL, OP_CLR_SCL,
	OP_WR_SDA, OP_SET_SCL, OP_CLR_SCL,
	OP_WR_SDA, OP_SET_SCL, OP_CLR_SCL,
	OP_WR_SDA, OP_SET_SCL, OP_CLR_SCL,
	OP_WR_SDA, OP_SET_SCL, OP_CLR_SCL,
	OP_WR_SDA, OP_SET_SCL, OP_CLR_SCL,
	OP_WR_SDA, OP_SET_SCL, OP_CLR_SCL,
	/* and read ack */
	OP_SET_SDA, OP_SET_SCL, OP_RD_SDA, OP_CLR_SCL,
	OP_DONE,
};

static const uint8_t i2c_op_read_byte[] = {
	OP_SET_SDA,
	OP_SET_SCL, OP_RD_SDA, OP_CLR_SCL,
	OP_SET_SCL, OP_RD_SDA, OP_CLR_SCL,
	OP_SET_SCL, OP_RD_SDA, OP_CLR_SCL,
	OP_SET_SCL, OP_RD_SDA, OP_CLR_SCL,
	OP_SET_SCL, OP_RD_SDA, OP_CLR_SCL,
	OP_SET_SCL, OP_RD_SDA, OP_CLR_SCL,
	OP_SET_SCL, OP_RD_SDA, OP_CLR_SCL,
	OP_SET_SCL, OP_RD_SDA, OP_CLR_SCL,
	/* and write ack */
	OP_ACK_SDA, OP_SET_SCL, OP_CLR_SCL,
	OP_DONE,
};

#define I2C_OP_FLAG_NAK 0x0001

static void i2c_op(struct i2c_dev *dev, enum i2c_op op, unsigned long flags)
{
	const struct i2c_cfg *cfg = dev->cfg;
	int opendrain = cfg->flags & I2C_FLAG_HW_OPENDRAIN;

	switch(op) {
	case OP_SET_SCL:
		if (opendrain)
			gpio_set(cfg->gpio_scl, 1);
		else
			gpio_dir(cfg->gpio_scl, GPIO_DIR_IN, 1);
		break;
	case OP_CLR_SCL:
		if (opendrain)
			gpio_set(cfg->gpio_scl, 0);
		else
			gpio_dir(cfg->gpio_scl, GPIO_DIR_OUT, 0);
		break;
	case OP_SET_SDA:
		if (opendrain)
			gpio_set(cfg->gpio_sda, 1);
		else
			gpio_dir(cfg->gpio_sda, GPIO_DIR_IN, 1);
		break;
	case OP_CLR_SDA:
		if (opendrain)
			gpio_set(cfg->gpio_sda, 0);
		else
			gpio_dir(cfg->gpio_sda, GPIO_DIR_OUT, 0);
		break;
	case OP_WR_SDA:
		if (dev->value & 0x80)
			i2c_op(dev, OP_SET_SDA, flags);
		else
			i2c_op(dev, OP_CLR_SDA, flags);
		dev->value <<= 1;
		break;
	case OP_RD_SDA: /* this is after "SET_SDA" for sure */
		dev->value <<= 1;
		dev->value |= (gpio_get(cfg->gpio_sda) != 0);
		break;
	case OP_ACK_SDA: /* we must set if last read is over */
		if (flags & I2C_OP_FLAG_NAK)
			i2c_op(dev, OP_SET_SDA, flags);
		else
			i2c_op(dev, OP_CLR_SDA, flags);
		break;
	case OP_DONE:
		break;
	}
}

/*
 * Each API function is a sequence of operation-sets
 */
struct i2c_action {
	int mode;
	const uint8_t *oparray;
};

enum i2c_mode{
	I2C_MODE_SINGLE = 1,
	I2C_MODE_LOOP,
	I2C_MODE_END = 0
};

static struct i2c_action i2c_act_reset[] = {
	{I2C_MODE_SINGLE, i2c_op_reset},
	{I2C_MODE_END}
};

static struct i2c_action i2c_act_write[] = {
	{I2C_MODE_SINGLE, i2c_op_start},
	{I2C_MODE_SINGLE, i2c_op_write_byte}, /* single byte == address */
	{I2C_MODE_LOOP, i2c_op_write_byte}, /* multi byte == buffer */
	{I2C_MODE_SINGLE, i2c_op_stop},
	{I2C_MODE_SINGLE, i2c_op_clk1},
	{I2C_MODE_END}
};

static struct i2c_action i2c_act_read[] = {
	{I2C_MODE_SINGLE, i2c_op_start},
	{I2C_MODE_SINGLE, i2c_op_repstart},
	{I2C_MODE_SINGLE, i2c_op_write_byte},
	{I2C_MODE_LOOP, i2c_op_read_byte}, /* multi byte == buffer */
	{I2C_MODE_SINGLE, i2c_op_stop},
	{I2C_MODE_SINGLE, i2c_op_clk1},
	{I2C_MODE_END}
};

/* Follow the action above */
static int i2c_act_step(struct i2c_dev *dev, struct i2c_action *act,
			 uint8_t addr8, uint8_t *buf, int count,
			 unsigned long flags)
{
	unsigned long opflags;

	if (dev->actindex < 0) {
		/* begin */
		dev->actindex = dev->bufindex = dev->opindex = 0;
	}
	/* if it's a new action (operation 0) check flags and termination */
	if (dev->opindex == 0) {
		if (act[dev->actindex].oparray == i2c_op_start
		    && ((flags & I2C_FLAG_NO_START)
			|| (flags & I2C_FLAG_REPSTART)))
			dev->actindex++;
		if (act[dev->actindex].oparray == i2c_op_repstart
		    && !(flags & I2C_FLAG_REPSTART))
			dev->actindex++;
		if (act[dev->actindex].oparray == i2c_op_stop
		    && flags & I2C_FLAG_NO_STOP)
			dev->actindex++;

		switch (act[dev->actindex].mode) {
		case I2C_MODE_END:
			dev->actindex = -1;
			return 0; /* success */
		case I2C_MODE_SINGLE:
			dev->value = addr8;
			break;
		case I2C_MODE_LOOP:
			dev->value = buf[dev->bufindex];
			break;
		}
	}

	opflags = 0;
	if (dev->bufindex == count - 1)
		opflags |= I2C_OP_FLAG_NAK;

	if (0)
		printf("op: actindex %i, oparray %p, opindex %i\n",
		       dev->actindex, act[dev->actindex].oparray, dev->opindex);
	i2c_op(dev, act[dev->actindex].oparray[dev->opindex++], opflags);

	/* If this operation array is over,  move on or loop */
	if (act[dev->actindex].oparray[dev->opindex] == OP_DONE) {
		switch (act[dev->actindex].mode) {
		case I2C_MODE_END:
			break; /* impossible */
		case I2C_MODE_SINGLE:
			dev->actindex++;
			dev->opindex = 0;
			break;
		case I2C_MODE_LOOP:
			dev->opindex = 0;

			/* save just-read byte, or prepare next-to-wr */
			if (act[dev->actindex].oparray == i2c_op_read_byte)
				buf[dev->bufindex++] = dev->value;
			else
				dev->value = buf[++dev->bufindex];

			if (dev->bufindex == count) {
				dev->actindex++;
				dev->bufindex = 0;
				break;
			}
			break;
		}
	}
	return -EAGAIN; /* loop in the caller */
}

static int i2c_action(struct i2c_dev *dev, struct i2c_action *act,
		      uint8_t addr8, uint8_t *buf, int count,
		      unsigned long flags)
{
	int ret = -EAGAIN;

	while (ret == -EAGAIN) {
		ret = i2c_act_step(dev, act, addr8, buf, count, flags);
		if (dev->cfg->flags & I2C_FLAG_HW_NONBLOCK)
			break;
		udelay(dev->cfg->usec_delay);
	}
	return ret;
}

/* external api */
int i2c_reset(struct i2c_dev *dev)
{
	return i2c_action(dev, i2c_act_reset, 0, NULL, 0, 0);
}

int i2c_write(struct i2c_dev *dev, int addr7, const uint8_t *buf,
	      int count, unsigned long flags)
{
	return i2c_action(dev, i2c_act_write, addr7 * 2 + 0, (void *)buf,
			  count, flags);
}

int i2c_read(struct i2c_dev *dev, int addr7, uint8_t *buf, int count,
		    unsigned long flags)
{
	return i2c_action(dev, i2c_act_read, addr7 * 2 + 1, buf,
			  count, flags);
}

int i2c_readreg(struct i2c_dev *dev, uint8_t addr7, uint8_t reg)
{
	/* write nostop, then read repstart */
	static int ndone;
	int ret;
	static uint8_t buf[1];

	if (!ndone) {
		buf[0] = reg;
		ret = i2c_write(dev, addr7, buf, sizeof(buf), I2C_FLAG_NO_STOP);
		if (ret)
			return ret;
		ndone++;
		if (dev->cfg->flags & I2C_FLAG_HW_NONBLOCK)
			return -EAGAIN;
		udelay(dev->cfg->usec_delay);
	}
	ret = i2c_read(dev, addr7, buf, sizeof(buf), I2C_FLAG_NO_START
		| I2C_FLAG_REPSTART);
	if (ret)
		return ret;
	ndone = 0; /* start again next iteration */
	return buf[0];
}

int i2c_readregs(struct i2c_dev *dev, uint8_t addr7, uint8_t *buf, int nregs)
{
	/* write nostop, then read repstart */
	static int ndone;
	int ret;

	if (!ndone) {
		ret = i2c_write(dev, addr7, buf, 1, I2C_FLAG_NO_STOP);
		if (ret)
			return ret;
		ndone++;
		if (dev->cfg->flags & I2C_FLAG_HW_NONBLOCK)
			return -EAGAIN;
		udelay(dev->cfg->usec_delay);
	}
	ret = i2c_read(dev, addr7, buf+1, nregs, I2C_FLAG_NO_START
		| I2C_FLAG_REPSTART);
	if (ret)
		return ret;
	ndone = 0; /* start again next iteration */
	return buf[0];
}

int i2c_writereg(struct i2c_dev *dev, uint8_t addr7, uint8_t reg, uint8_t val)
{
	uint8_t buf[2] = {reg, val};

	return i2c_write(dev, addr7, buf, sizeof(buf), 0);
}

int i2c_writeregs(struct i2c_dev *dev, uint8_t addr7, uint8_t *buf, int nregs)
{
	return i2c_write(dev, addr7, buf, nregs + 1, 0);
}
