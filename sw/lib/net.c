/*
 * This work is part of the White Rabbit project
 *
 * Copyright (C) 2010 - 2015 GSI (www.gsi.de), CERN (www.cern.ch)
 * Author: Wesley W. Terpstra <w.terpstra@gsi.de>
 * Author: Tomasz Wlostowski <tomasz.wlostowski@cern.ch>
 * Author: Adam Wujek <adam.wujek@cern.ch>
 *
 * Released according to the GNU GPL, version 2 or any later version.
 */
#include <string.h>
#include <errno.h>
#include <ipv4.h>
#include <io.h>

static struct socket *socks[NET_MAX_SOCKETS];

struct socket *socket_create(struct socket *sock,
			     struct sockaddr *bind_addr,
			     int udp_or_raw, int udpport)
{
	int i;

	/* Look for the first available socket. */
	for (i = 0; i < ARRAY_SIZE(socks); i++)
		if (!socks[i]) {
			socks[i] = sock;
			break;
		}
	if (i == ARRAY_SIZE(socks)) {
		printf("%s: no socket slots left\n", __func__);
		return NULL;
	}
	net_verbose("%s: socket %p for %04x:%04x, slot %i\n", __func__,
		    sock, ntohs(bind_addr->ethertype),
		    udpport, i);

	/* copy and complete the bind information. If MAC is 0 use unicast */
	memset(&sock->bind_addr, 0, sizeof(struct sockaddr));
	if (bind_addr)
		memcpy(&sock->bind_addr, bind_addr, sizeof(struct sockaddr));
	sock->bind_addr.udpport = 0;
	if (udp_or_raw == SOCK_UDP) {
		sock->bind_addr.ethertype = htons(0x0800); /* IPv4 */
		sock->bind_addr.udpport = udpport;
	}

	/*get mac from endpoint */
	net_get_mac(sock->local_mac);

	/*packet queue */
	sock->queue.head = sock->queue.tail = 0;
	sock->queue.avail = sock->queue.size;
	sock->queue.n = 0;
	return sock;
}

int socket_close(struct socket *s)
{
	int i;
	for (i = 0; i < ARRAY_SIZE(socks); i++)
		if (socks[i] == s)
			socks[i] = NULL;
	return 0;
}

/* Slow, but we don't care much... */
static int wrap_copy_in(void *dst, struct sockq *q, int len, int buflen)
{
	char *dptr = dst;
	int i;

	if (!buflen)
		buflen = len;
	net_verbose("copy_in: tail %d avail %d len %d (buf %d)\n",
		    q->tail, q->avail, len, buflen);
	i = min(len, buflen);
	while (i--) {
		*dptr++ = q->buff[q->tail];
		q->tail++;
		if (q->tail == q->size)
			q->tail = 0;
	}
	if (len > buflen) {
		q->tail += len - buflen;
		while (q->tail > q->size)
			q->tail -= q->size;
	}
	return len;
}

static int wrap_copy_out(struct sockq *q, void *src, int len)
{
	char *sptr = src;
	int i = len;

	net_verbose("copy_out: head %d avail %d len %d\n", q->head, q->avail,
		   len);

	while (i--) {
		q->buff[q->head++] = *sptr++;
		if (q->head == q->size)
			q->head = 0;
	}
	return len;
}

int socket_recvfrom(struct socket *s, struct sockaddr *from,
                    void *data, int data_length)
{
	struct sockq *q = &s->queue;

	uint16_t size;
	struct ethhdr hdr;

	/*check if there is something to fetch */
	if (!q->n)
		return 0;

	q->n--;

	q->avail += wrap_copy_in(&size, q, 2, 0);
	q->avail += wrap_copy_in(&hdr, q, sizeof(struct ethhdr), 0);
	q->avail += wrap_copy_in(data, q, size, data_length);

	from->ethertype = ntohs(hdr.ethtype);
	from->vlan = vlan_number; /* has been checked in rcvd frame */
	memcpy(from->mac, hdr.srcmac, 6);
	memcpy(from->mac_dest, hdr.dstmac, 6);

	net_verbose("%s: called from %p\n",
		    __func__, __builtin_return_address(0));
	net_verbose("RX: Size %d tail %d Smac %x:%x:%x:%x:%x:%x\n", size,
		   q->tail, hdr.srcmac[0], hdr.srcmac[1], hdr.srcmac[2],
		   hdr.srcmac[3], hdr.srcmac[4], hdr.srcmac[5]);

	return min(size, data_length);
}

int socket_sendto(struct socket *sock, struct sockaddr *to,
		  void *data, int data_length)
{
	struct ethhdr_vlan hdr;
	int rval;

	memcpy(hdr.dstmac, to->mac, 6);
	memcpy(hdr.srcmac, sock->local_mac, 6);
	if (vlan_number) {
		hdr.ethtype = htons(0x8100);
		hdr.tag = htons(vlan_number); /* no priority bits */
		hdr.ethtype_2 = sock->bind_addr.ethertype; /* net order */
	} else {
		hdr.ethtype = sock->bind_addr.ethertype;
	}
	net_verbose("TX: socket %04x:%04x, len %i\n",
		    ntohs(sock->bind_addr.ethertype),
		    sock->bind_addr.udpport,
		    data_length);

	rval = frame_tx(&hdr, (uint8_t *) data, data_length);
	return rval;
}

/* This must be polled repeatedly */
int net_update_rx_queues(void)
{
	struct socket *s = NULL, *raws = NULL, *udps = NULL;
	struct sockq *q;
	static struct ethhdr hdr;
	int recvd, i, q_required;
	static uint8_t buffer[NET_MAX_SKBUF_SIZE - 32];
	uint8_t *payload = buffer;
	uint16_t size, port;
	uint16_t ethtype, tag;

	recvd = frame_rx(&hdr, buffer, sizeof(buffer));

	if (recvd <= 0)		/* No data received? */
		return 0;

	/* Remove the vlan tag, but  make sure it's the right one */
	ethtype = hdr.ethtype;
	tag = 0;
	if (ntohs(ethtype) == 0x8100) {
		memcpy(&tag, buffer, 2);
		memcpy(&hdr.ethtype, buffer + 2, 2);
		payload += 4;
		recvd -= 4;
	}
	if ((ntohs(tag) & 0xfff) != vlan_number) {
		net_verbose("%s: want vlan %i, got %i: discard\n",
				    __func__, vlan_number,
				    ntohs(tag) & 0xfff);
			return 0;
	}

	/* Prepare for IP/UDP checks */
	if (payload[IP_VERSION] == 0x45 && payload[IP_PROTOCOL] == 17)
		port = payload[UDP_DPORT] << 8 | payload[UDP_DPORT + 1];
	else
		port = 0;

	for (i = 0; i < ARRAY_SIZE(socks); i++) {
		s = socks[i];
		if (!s)
			continue;
		if (hdr.ethtype != s->bind_addr.ethertype)
			continue;
		if (!port && !s->bind_addr.udpport)
			raws = s; /* match with raw socket */
		if (port && s->bind_addr.udpport == port)
			udps = s; /*  match with udp socket */
	}
	s = udps;
	if (!s)
		s = raws;
	if (!s) {
		net_verbose("%s: could not find socket for packet\n",
			   __FUNCTION__);
		return 1;
	}

	q = &s->queue;
	q_required =
	    sizeof(struct ethhdr) + recvd + 2;

	if (q->avail < q_required) {
		net_verbose
		    ("%s: queue for socket full; [avail %d required %d]\n",
		     __FUNCTION__, q->avail, q_required);
		return 1;
	}

	size = recvd;

	q->avail -= wrap_copy_out(q, &size, 2);
	q->avail -= wrap_copy_out(q, &hdr, sizeof(struct ethhdr));
	q->avail -= wrap_copy_out(q, payload, size);
	q->n++;

	net_verbose("Q: Size %d head %d Smac %x:%x:%x:%x:%x:%x\n", recvd,
		   q->head, hdr.srcmac[0], hdr.srcmac[1], hdr.srcmac[2],
		   hdr.srcmac[3], hdr.srcmac[4], hdr.srcmac[5]);

	net_verbose("%s: saved packet to socket %04x:%04x "
		    "[avail %d n %d size %d]\n", __FUNCTION__,
		    ntohs(s->bind_addr.ethertype),
		    s->bind_addr.udpport,
		    q->avail, q->n, q_required);
	return 1;
}
