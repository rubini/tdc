#include <init.h>
#include <time.h>
#include <io.h>

static int timer_setup(void)
{
	int prvalue;

	regs[REG_AHBCLKCTRL] |= REG_AHBCLKCTRL_CT32B1;

	prvalue = (CPU_FREQ / HZ) - 1;
	if (CONFIG_CPU_IS_17xx) {
		/* unless we change PCLKSEL0, by default we run at 1/4f */
		prvalue = (CPU_FREQ / 4 / HZ) -1;
	}

	/* enable timer 1, and count at HZ Hz (e.g.  1000) */
	regs[REG_TMR32B1TCR] = 1;
	regs[REG_TMR32B1TCR] = 3;
	regs[REG_TMR32B1TCR] = 1;
	regs[REG_TMR32B1PR] = prvalue;
	return 0;
}

core_initcall(timer_setup);

