#include <io.h>
#include <errno.h>

void putc(int c)
{
	if (c == '\n')
		putc('\r');
	while ( !(regs[REG_UCSR0A] & REG_UCSR0A_UDRE) )
		;
	regs[REG_UDR0] = c;
}

/* returns negative on failure. But \r\n is two bytes, remember it */
static int pending = -1;
int try_putc(int c)
{

	if ( !(regs[REG_UCSR0A] & REG_UCSR0A_UDRE) )
		return -1;
	if (pending >= 0) {
		regs[REG_UDR0] = pending;
		pending = -1;
		return 0;
	}
	if (c == '\n') {
		pending = c;
		regs[REG_UDR0] = '\r';
		return -1;
	}
	regs[REG_UDR0] = c;
	return 0;
}

/* getc is blocking */
int getc(void)
{
	while (!(regs[REG_UCSR0A] & REG_UCSR0A_RXC))
		;
	return regs[REG_UDR0];
}

/* non-blocking serial input, returns NULL or -EAGAIN if not ready */
int pollc(void)
{
	int ret = -EAGAIN;
	while (!(regs[REG_UCSR0A] & REG_UCSR0A_RXC))
		return regs[REG_UDR0];
	return ret;
}

