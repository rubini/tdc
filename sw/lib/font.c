#include <font.h>
#include <panic.h>

const uint8_t *font_get(const struct font *f, int asc)
{
	int csize;
	const uint8_t *fail;

	if (f->flags & FONT_FLAG_V)
		csize = f->wid * ((f->hei + 7) / 8);
	else
		csize = f->hei * ((f->wid + 7) / 8);

	fail = f->data + csize * (f->last + 1 - f->first);

	if (asc < f->first || asc > f->last)
		return fail;
	return f->data + csize * (asc - f->first);
}

const uint8_t *font_get_hv(const struct font *f, int asc, int flags)
{
	static uint8_t gl_target[8];
	const uint8_t *gl_orig;
	int dest, src, destbit, srcbit, val;
	const int hvflags = FONT_FLAG_H | FONT_FLAG_V;

	gl_orig = font_get(f, asc);
	if (f->flags == flags)
		return gl_orig;

	if (f->wid > 8  || f->hei > 8)
		panic(0xff, "Can't convert font %ix%i\n", f->wid, f->hei);

	/* So, we can be flipped or just bitorder-wrong */
	if ((f->flags & hvflags) == (flags & hvflags)) {
		/* Only bit order */
		for (dest = 0; dest < 8; dest++) {
			val = 0;
			for (src = 0; src < 8; src++)
				if (gl_orig[dest] & (1 << src))
					val |= 0x80 >> src;
			gl_target[dest] = val;
		}
		return gl_target;
	}

	/* Flip x/y and possibly bit-order too */
	for (dest = 0; dest < 8; dest++) {
		srcbit = 0x80 >> dest;
		val = 0;
		for (src = 0; src < 8; src++) {
			destbit = (flags & FONT_FLAG_LSB)
				? 0x01 << src : 0x80 >> src;
			if (gl_orig[src] &  srcbit)
				val |= destbit;
		}
		gl_target[dest] = val;
	}
	return gl_target;
}
