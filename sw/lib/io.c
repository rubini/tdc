#include <io.h>
#include <errno.h>

int puts_is_buffered;

static char puts_buffer[CONFIG_PUTS_BUFFER];
static unsigned nr, nw;

void puts_poll(void)
{
	if (nr == nw)
		return;
	if (try_putc(puts_buffer[nr % CONFIG_PUTS_BUFFER]) < 0)
		return;
	nr++;
}

static int _puts(const char *s)
{
	while (*s)
		putc (*s++);
	return 0;
}

static int b_puts(const char *s)
{
	/* prefer new bytes, push ahead nr on overflow and warn*/
	while (*s)
		puts_buffer[nw++ % CONFIG_PUTS_BUFFER] = *s++;
	if (nw - nr > CONFIG_PUTS_BUFFER) {
		nr = nw - CONFIG_PUTS_BUFFER;
		puts_buffer[nr % CONFIG_PUTS_BUFFER] = '!';
	}
	if (0) {
		char b[64];
		sprintf(b, "<b_puts: Now: nr %i nw %i>", nr, nw);
		_puts(b);
	}
	return 0;
}

int uart_puts(const char *s)
{
	if (CONFIG_PUTS_BUFFER > 0 && puts_is_buffered)
		return b_puts(s);
	return _puts(s);
}

int __attribute__((weak,alias("uart_puts"))) puts(const char *s);

/* gets is blocking, because getc is */
char *gets(char *s)
{
	int c, i = 0;
	do {
		s[i++] = c = getc();
		s[i] = '\0';
	}
	while (c != '\n' && c != '\r');
	return s;
}

/* polls is non-blocking */
char *polls(char *s, int bsize)
{
	return polls_t(s, "\n\r", bsize);
}

char *polls_t(char *s, char *terminators, int bsize)
{
	int c, t, i = 0;

	if (bsize < 2)
		return NULL;
	c = pollc();
	if (c == -EAGAIN)
		return NULL; /* no news since last call */

	/* go to end of currently-accumulated string */
	while (s[i] && i < bsize - 1)
		i++;
	if (i == bsize - 1) /* caller made something wrong, start fresh */
		i = 0;
	s[i++] = c;
	s[i] = '\0';
	if (i == bsize - 1)
		return s;
	for (t = 0; terminators[t]; t++)
		if (c == terminators[t])
			return s;
	return NULL;
}
