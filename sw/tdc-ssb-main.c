#include <io.h>
#include <time.h>
#include <string.h>
#include <revision.h>

/*
 * TDC Simple Serial Buffer. Simple but with final frame format
 *
 * Internal (by nibble):  TTTTTttt SSMMMmmm
 *    T = timestamp before (20 bits)
 *    t = timestamp after (12 bits)
 *    S = sequence number
 *    M = mask before (12 bits, 9 meaningful)
 *    m = mask after (12 bits, 9 meaningful)
 *
 * External:  TTTTT-ttt:SS:MMM-mmm\n
 */

#if CONFIG_HZ != 1000 * 1000
#warning "HZ must be 1 million, for 1us resolution"
#endif

#define TDC_DEBUG GPIO_NR(0, 11)

/* Let's have a buffer. Not a power of two, so save next index */
struct tdc_sample {
	unsigned long sequence;
	uint32_t w1, w2;
	int next;
};
static struct tdc_sample tdc_sample[63];

/* And a string for the ongoing sample, with string index */
static char tdc_string[32];
static int tdc_string_offset = 0;
static int tdc_strlen;

/* Print to the string, and reset string offset */
static void tdc_print(uint32_t w1, uint32_t w2)
{
	unsigned int i1 = w1, i2 = w2; /* prevent printf warning */
	/* Print to a buffer, and uart is being polled */
	sprintf(tdc_string, "%05x-%03x:%02x:%03x-%03x\r\n",
		i1 >> 12, i1 & 0xfff, i2 >> 24, (i2 >> 12) & 0xfff, i2 & 0xfff);
}

/* Save a new sample */
static void tdc_save(unsigned long sequence, uint32_t w1, uint32_t w2)
{
	static int index;
	struct tdc_sample *s = tdc_sample + index;

	//printf("save: %08lx -> %i\n", sequence, index);
	s->sequence = sequence;
	s->w1 = w1;
	s->w2 = w2;
	index = s->next;
}

/* Poll for uart, print out stuff */
static void tdc_poll(void)
{
	static int index;
	struct tdc_sample *s = tdc_sample + index;
	static unsigned long last_sequence;

	if ( !(regs[REG_U0LSR] & REG_U0LSR_THRE) )
		return; /* tx busy */
	if (tdc_string_offset < tdc_strlen) {
		regs[REG_U0THR] = tdc_string[tdc_string_offset++];
		return;
	}
	/* Do we need a new sample? */
	if (time_before_eq(s->sequence, last_sequence))
		return;
	last_sequence = s->sequence;
	tdc_print(s->w1, s->w2); /* This takes 131us @48MHz */
	tdc_string_offset = 0;
	index = s->next;
}

/* And run it all */
void main(void)
{
	uint32_t omask, nmask, oj, nj;
	uint32_t w1, w2;
	unsigned long sequence = 1;
	int i;

	printf("TDC Simple Serial Buffer\n");
	printf("Build: %s\n   (on %s, %s)\n", __gitc__, __date__, __time__);
	gpio_dir_af(TDC_DEBUG, GPIO_DIR_OUT, 0, GPIO_AF_GPIO);

	/* Initialize the circular buffer with proper next index */
	for (i = 0; i < ARRAY_SIZE(tdc_sample) -1; i++)
		tdc_sample[i].next = i+1;
	tdc_sample[i].next = 0;

	/* Calc strlen and prime variable in "waiting for data" state */
	tdc_print(0,0);
	tdc_strlen = strlen(tdc_string);
	tdc_string_offset = tdc_strlen;

	/*
	 * We kwow we are 11U24 (newer gpio), so read our 9-bit mask.
	 * We need pio0 12-16 and 20-23; ignore other ones
	 */
	omask = 0x00f1f000; /* our mask of valid bits */
	for (i = 0; i < 31; i++)
		if (omask & (1 << i))
			gpio_dir_af(GPIO_NR(0, i),
				    GPIO_DIR_IN, 0,
				    GPIO_AF_GPIO | GPIO_AF_PULLDOWN);
	writel(~omask, __GPIO_MASK(0));

	nj = jiffies;
	nmask = readl(__GPIO_MPIN(0));
	while (1) {
		gpio_set(TDC_DEBUG, 0);
		oj = nj;
		omask = nmask;
		nj = jiffies;
		nmask = readl(__GPIO_MPIN(0));
		gpio_set(TDC_DEBUG, 1);
		if (nmask == omask) {
			tdc_poll();
			continue;
		}
		/* save below takes 1 microsecond, more or less */
		w1 = (oj << 12) | (nj & 0xfff);
		w2 = (sequence << 24) | omask | (nmask >> 12);
		tdc_save(sequence, w1, w2);
		sequence++;
	}
}
