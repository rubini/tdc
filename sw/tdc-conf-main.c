#include <io.h>
#include <time.h>
#include <string.h>
#include <revision.h>

/*
 * TDC Configurable application.
 *
 * Internal long (by nibble):  TTTTTttt SSMMMmmm
 *    T = timestamp before (20 bits)
 *    t = timestamp after (12 bits)
 *    S = sequence number
 *    M = mask before (12 bits, 9 meaningful)
 *    m = mask after (12 bits, 9 meaningful)
 *
 * External long:  TTTTT-ttt:SS:MMM-mmm\n
 *
 * Internal short (by nibble): tttttmmm SSSSSSSS

 * External short: SStttttmmm\n
 */

#if CONFIG_HZ != 1000 * 1000
#warning "HZ must be 1 million, for 1us resolution"
#endif

#define TDC_DEBUG GPIO_NR(0, 11)

/* Let's have a buffer. Not a power of two, so save next index */
struct tdc_sample {
	unsigned long sequence;
	uint32_t w1, w2;
	int next;
};
static struct tdc_sample tdc_sample[CONFIG_TDC_N_EVENTS];

/* And a string for the ongoing sample, with string index */
static char tdc_string[32];
static int tdc_string_offset = 0;
static int tdc_strlen;

/*
 * Printing with sprintf took 130usecs. Too much. Split it into steps
 */
uint8_t tdc_hex[] = "0123456789abcdef";
uint8_t tdc_sep[] = "-:\r\n";

struct tdc_print_step {
	int windex, bitshift;
	uint8_t *array;
};
struct tdc_print_step tdc_print_steps_long[] = {
	{0, 28, tdc_hex},
	{0, 24, tdc_hex},
	{0, 20, tdc_hex},
	{0, 16, tdc_hex},
	{0, 12, tdc_hex},
	{2,  0, tdc_sep}, /* - */
	{0,  8, tdc_hex},
	{0,  4, tdc_hex},
	{0,  0, tdc_hex},
	{2,  4, tdc_sep}, /* : */
	{1, 28, tdc_hex},
	{1, 24, tdc_hex},
	{2,  4, tdc_sep}, /* : */
	{1, 20, tdc_hex},
	{1, 16, tdc_hex},
	{1, 12, tdc_hex},
	{2,  0, tdc_sep}, /* - */
	{1,  8, tdc_hex},
	{1,  4, tdc_hex},
	{1,  0, tdc_hex},
	{2,  8, tdc_sep}, /* \r */
	{2, 12, tdc_sep}, /* \n */
	{0,},
};
/* Short version: w1 is jiffies+mask, w2 is serialnr. Spit "SSTTTTTMMM\n" */
struct tdc_print_step tdc_print_steps_short[] = {
	{1,  4, tdc_hex},
	{1,  0, tdc_hex},
	{0, 28, tdc_hex},
	{0, 24, tdc_hex},
	{0, 20, tdc_hex},
	{0, 16, tdc_hex},
	{0, 12, tdc_hex},
	{0,  8, tdc_hex},
	{0,  4, tdc_hex},
	{0,  0, tdc_hex},
	{2, 12, tdc_sep}, /* \n */
	{0,},
};
struct tdc_print_step *tdc_steps;

static inline int tdc_print(uint32_t w1, uint32_t w2)
{
	static uint32_t w[3] = {0, 0, 0x3210 /* separators */};
	struct tdc_print_step *s;
	static int step;

	if (tdc_string_offset == tdc_strlen) {
		tdc_string_offset = 0;
		//printf("prepare words %08x %08x\n", w1, w2);
		w[0] = w1; w[1] = w2; step = 0;
		return 1;
	}
	s = tdc_steps + step;
	if (0) {
		printf("step %i, s %p, windex %i (%08x), shift %i, array %p: ",
		       step, s, s->windex, (int)w[s->windex],
		       s->bitshift, s->array);
	}
	if (!s->array)
		return 0;
	tdc_string[step] = s->array[ (w[s->windex] >> s->bitshift) & 0x0f ];
	if (0) printf("%02x\n", tdc_string[step]);
	step++;
	return 1;
}

/* Save a new sample */
static inline void tdc_save(unsigned long sequence, uint32_t w1, uint32_t w2)
{
	static int index;
	struct tdc_sample *s = tdc_sample + index;

	//printf("save: %08lx -> %i\n", sequence, index);
	s->sequence = sequence;
	s->w1 = w1;
	s->w2 = w2;
	index = s->next;
}

/* Poll for uart, print out stuff */
static inline void tdc_poll(void)
{
	static int index;
	struct tdc_sample *s = tdc_sample + index;
	static unsigned long last_sequence;
	static int printing;

	if (printing) {
		printing = tdc_print(0, 0 /* both unused */);
		return;
	}
	if ( !(regs[REG_U0LSR] & REG_U0LSR_THRE) )
		return; /* tx busy */
	if (tdc_string_offset < tdc_strlen) {
		regs[REG_U0THR] = tdc_string[tdc_string_offset++];
		return;
	}
	/* Do we need a new sample? */
	if (time_before_eq(s->sequence, last_sequence))
		return;
	//printf("next sequence: %li, last %li\n", s->sequence, last_sequence);
	last_sequence = s->sequence;
	printing = tdc_print(s->w1, s->w2);
	index = s->next;
	return;

	//printf ("want to print \"%s\"\n", tdc_string);
}

static void __attribute__((section(".ramcode"), noinline, noreturn)) loop(void)
{
	uint32_t omask, nmask, omask_out, nmask_out, oj, nj;
	uint32_t w1, w2;
	unsigned long sequence = 1;

	nj = jiffies;
	nmask = readl(__GPIO_MPIN(0));
	while (1) {
		__gpio_set(TDC_DEBUG, 0);
		oj = nj;
		omask = nmask;
		nj = jiffies;
		nmask = readl(__GPIO_MPIN(0));
		__gpio_set(TDC_DEBUG, 1);
		if (nmask == omask) {
			tdc_poll();
			continue;
		}
		if (CONFIG_TDC_IS_BOTH_EDGES) {
			omask_out = omask;
			nmask_out = nmask;
		} else {
			/* Only report raising edge. More tricky */
			omask_out = 0;
			nmask_out = (nmask ^ omask) & nmask;
			if (!nmask_out)
				continue;
		}
		if (CONFIG_TDC_IS_SHORT) {
			w1 = (nj << 12) | (nmask_out >> 12);
			w2 = sequence;
		} else {
			/* save below takes 1 microsecond, more or less */
			w1 = (oj << 12) | (nj & 0xfff);
			w2 = (sequence << 24) | omask_out | (nmask_out >> 12);
		}
		tdc_save(sequence, w1, w2);
		sequence++;
	}
}

/* And run it all */
void main(void)
{
	int i, mask;

	printf("TDC Configurable binary\n");
	printf("Build: %s\n   (on %s, %s)\n", __gitc__, __date__, __time__);
	printf("   Event buffer size: %i\n", CONFIG_TDC_N_EVENTS);
	gpio_dir_af(TDC_DEBUG, GPIO_DIR_OUT, 0, GPIO_AF_GPIO);

	if (CONFIG_TDC_IS_SHORT) {
		printf("   short message\n");
		tdc_steps = tdc_print_steps_short;
		tdc_strlen = ARRAY_SIZE(tdc_print_steps_short) - 1;
	} else {
		printf("   long message\n");
		tdc_steps = tdc_print_steps_long;
		tdc_strlen = ARRAY_SIZE(tdc_print_steps_long) - 1;
	}

	/* Initialize the circular buffer with proper next index */
	for (i = 0; i < ARRAY_SIZE(tdc_sample) -1; i++)
		tdc_sample[i].next = i+1;
	tdc_sample[i].next = 0;

	/* Prime variable in "waiting for data" state */
	tdc_string_offset = tdc_strlen;

	/*
	 * We kwow we are 11U24 (newer gpio), so read our 9-bit mask.
	 * We need pio0 12-16 and 20-23; ignore other ones
	 */
	mask = 0x00f1f000; /* our mask of valid bits */
	for (i = 0; i < 31; i++)
		if (mask & (1 << i))
			gpio_dir_af(GPIO_NR(0, i),
				    GPIO_DIR_IN, 0, GPIO_AF_GPIO
				    | GPIO_AF_PULLDOWN);
	writel(~mask, __GPIO_MASK(0));

	loop();
}
