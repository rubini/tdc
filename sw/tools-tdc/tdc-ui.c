#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/time.h>
#define ARRAY_SIZE(x) (sizeof(x)/sizeof(x[0]))

int bits[] = {
	0x10, 0x1, 0x2, 0x4, 0x8, 0x100, 0x200, 0x400, 0x800
};
char *names[] = {
	"pps", "0", "1", "2", "3", "4", "5", "6", "7"
};

unsigned long long last, chlast[ARRAY_SIZE(names)], chraise[ARRAY_SIZE(names)];

int main(int argc, char **argv)
{
	unsigned long long t0, t1, t0prev = 0;
	unsigned long hibits = 0, second = 0, m0, m1;
	int seq, nexts = -1;
	FILE *f = stdin;
	char s[80];
	int first_ch = 1;

	if (getenv("TDC_VERBOSE"))
		first_ch = 0; /* report pps too */

	if (argc > 1)
		f = fopen(argv[1], "r");
	if (!f) {
		fprintf(stderr, "%s: %s: %s\n", argv[0], argv[1],
			strerror(errno));
		exit(1);
	}
	setlinebuf(f);

	t0prev = second = 0;
	while (fgets(s, sizeof(s), f)) {
		int i, mask;
		static int omask;

		if (sscanf(s, "%llx-%llx:%x:%lx-%lx",
			   &t0, &t1, &seq, &m0, &m1) != 5) {
			/* try short format */
			if (sscanf(s, "%2x%5llx%lx", &seq, &t1, &m1) != 3)
				continue;
			t0 = t1;
			m0 = omask;
			omask = m1;
		}

		/* report lost events errors, assume buffer size is 127 */
		if (nexts != -1 && seq != nexts) {
			int lost;

			for (lost = 0; (nexts & 0xff) != seq; nexts += 127)
				lost += 127;
			printf("lost %i events\n", lost);
		}
		nexts = (seq + 1) & 0xff;

		/* t0 is 5 digits, t1 is 3 digits, for sure */
		t1 = (t0 & 0xff000) | t1;
		if (t1 < t0) t1 += 0x1000;

		/* manage hibits */
		if (t0 < t0prev)
			hibits += 0x100000;
		t0prev = t0;
		t0 += hibits;
		t1 += hibits;

		mask = m0 ^ m1; /* what changed */
		for (i = first_ch; i < ARRAY_SIZE(bits); i++) {
			unsigned long long t2, t = t1 - second;

			if (!(mask & bits[i]))
				continue;
			printf("%4s: %s @ %3lli.%06lli", names[i],
			       m1 & bits[i] ? "raise" : "fall ",
			       t / 1000 / 1000, t % (1000 * 1000));

			/* Print deltas too -- first one is wrong */
			t = t1 - last;
			t2 = t1 - chlast[i];
			printf("   delta: %2lli.%06lli %3lli.%06lli",
			       t / 1000 / 1000, t % (1000 * 1000),
			       t2 / 1000 / 1000, t2 % (1000 * 1000));

			/* And frequency, if raising edge */
			if (m1 & bits[i]) {
				t2 = t1 - chraise[i];
				t = 1000 * 1000 * 1000 / t2;
				printf("    f %5lli.%03lli\n",
				       t / 1000, t % 1000);
				chraise[i] = t1;
			} else {
				printf("\n");
			}

			last = t1;
			chlast[i] = t1;
		}
		if ((m1 & 0x10) && !(m0 & 0x10))
			second = t1;
	}
	return 0;
}
