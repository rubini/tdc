
static void timer_setup(void)
{
	/* 16M / 256 / 256 = 244.14 Hz -- or 122.07 at 8MHz... */
	regs[REG_TCCR2B] = REG_TCCR2B_P256;
	regs[REG_TIMSK2] = REG_TIMSK2_TOIE2;

	asm("sei"); /* enable interrupts */
}

void setup(void)
{
	extern void do_initcalls(void);
	extern void uart_setup(void);

	timer_setup();
	uart_setup();
	do_initcalls();
}
