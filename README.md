
# tdc

This project is what I prepared for the "linuxlab 2018" conference.

It includes the hardware project, the software project and the slides.

## Writing firmware to the board 

If you have one of these boards, you can program the firmware using
the USB port (you need to power it using USB, not the 12V socket).

If signal PRG* is low when RST* goes high, the internal ROM will
manage programming, presenting itself as a storage device.  A
brand-new device will enter programming regardless of PRG*, as no
valid program is detected in flash memory.

The preferred way to copy the binary is through mcopy, part of mtools:
mounting the drive and copying will most likely not work (and I won't
bore you with the reason of this).  Thus, you should prepare your
system-wide /etc/mtools.conf or personal .mtoolsrc to access the
proper block device for the target CPU.

Thus, I personally have several entries in my /etc/mtools.conf to
manage my use of microcontrollers:

    drive b: file="/dev/sdb" exclusive
    drive c: file="/dev/sdc" exclusive
    drive d: file="/dev/sdd" exclusive
    drive e: file="/dev/sde" exclusive

The binary file must be written to the target USB storage using any
filename, but is always read back as firmware.bin.  Therefore, you
should either erase firmware.bin first or overwrite it with the same
name.  This is the preferred command line to overwrite (assuming b: is
the proper drive name):

    mcopy -o moth.bin b:firmware.bin

Here "-o" means overwrite, but please check that this works with your
own version of "mcopy" (or try without -o and see the error message).

Then, reset the CPU again to run the new program.
